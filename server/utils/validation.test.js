const expect = require('expect');
const {isRealString} = require('./validation');

describe('Testing is real string', () => {
    it('should reject nonstring values' , () => {
        let str = 123;
        let res = isRealString(str);
        expect(res).toBe(false);
    });
    it('should reject string with only spaces' , () => {
        let str = '     ';
        let res = isRealString(str);
        expect(res).toBe(false);
    });
    it('should return true if string with non space characters was passed' , () => {
        let str = 'This is a string';
        let res = isRealString(str);
        expect(res).toBe(true)
    });
})