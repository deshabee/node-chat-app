const expect = require('expect');
const {generateMessage , generateLocationMessage} = require('./message');

describe('generateMessage' , () => {
    it('should generate correct message object' , () => {
        var from,text,message;
        from= 'desha@example.com';
        text= 'This is the first message test';
        message = generateMessage(from , text);
        expect(message.from).toBe(from);
        expect(message.createdAt).toBeA('number');
    });
});

describe('generateLocationMessage' , () => {
    it('should generate correct message object' , () => {
        var from,long,lat;
        from= 'desha@example.com';
        long= 9000;
        lat= 9001;
        message = generateLocationMessage(from ,lat ,long);
        expect(message.from).toBe(from);
        expect(message.createdAt).toBeA('number');
        expect(message.url).toBe(`https://www.google.com/maps?q=${lat},${long}`);
    });
});