const path = require('path');
const express = require('express');
const socketIO = require('socket.io');
const http = require('http');
// const moment = require('moment');

const {generateMessage,generateLocationMessage} = require('./utils/message');
const {isRealString} = require('./utils/validation');
const publicPath = path.join(__dirname , '../public');
var port = process.env.PORT || 3000;
var app = express();
var server = http.createServer(app);
var io = socketIO(server);

io.on('connection' , (socket) => {
    console.log("a user is connected");

    socket.emit('newMessage' ,generateMessage('Admin','Welcome to the chat app'));

    socket.broadcast.emit('newMessage' ,generateMessage('Admin','New user joined'));

    socket.on('join' , (params , callback) => {
        if(!isRealString(params.name) || !isRealString(params.room)){
            callback('Name or room name is not correct');
        }
        callback();
    });

    socket.on('disconnect' , () => {
        console.log('disconnected');
    });

    socket.on('createMessage' , (body,callback) => {
        io.emit('newMessage' ,generateMessage(body.from,body.text));
        callback();
    });

    socket.on('createLocationMessage' , (body) => {
        io.emit('newLocationMessage' , generateLocationMessage('Admin' , body.latitude , body.longitude));
    })
});

app.use(express.static(publicPath));


server.listen(port , () => {
    console.log(`Our App is listening now on port ${port}`);
})

