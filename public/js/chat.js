var socket = io();

const scrollToBottom = () => {
    var messages = $('#messages');
    var newMessage = messages.children('li:last-child');

    var newMessageHeight = newMessage.innerHeight();
    var lastMessageHeight = newMessage.prev().innerHeight();
    var clientHeight = messages.prop('clientHeight');
    var scrollTop = messages.prop('scrollTop');
    var scrollHeight = messages.prop('scrollHeight');

    if(clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight){
        messages.scrollTop(scrollHeight);
    }
};

socket.on('connect', () => {
    var params = $.deparam(window.location.search);
    socket.emit('join' , params , (err) => {
        if (err) {
            alert(err);
            window.location.href = '/';
        } else {
            console.log('No error');
        }
    });
    console.log('connected to a server');
});
socket.on('newMessage', (body) => {
    let formattedTime = moment(body.createdAt).format('hh:mm a');
    var template = $('#message-template').html();
    var html = Mustache.render(template , {
        text: body.text,
        from: body.from,
        createdAt: formattedTime
    });
    $('#messages').append(html);
    scrollToBottom();
});

socket.on('disconnect', () => {
    console.log('Disconnected from server')
});

$(' #message-form ').on('submit' , (e) => {
    e.preventDefault();
    let messageTextbox = $(' #message-input ');
    socket.emit('createMessage' , {
        from: 'User',
        text: messageTextbox.val()
    }, (ack) => {
        messageTextbox.val('');
    })  
});

var locationButton = $('#send-location');
locationButton.on('click' , () => {
    if(!navigator.geolocation) {
        return alert('Please enable the geolocation');
    }

    locationButton.attr('disabled' , 'disabled').text('Sending location....');
    navigator.geolocation.getCurrentPosition((pos) => {
        let latitude, longitude;
        latitude= pos.coords.latitude;
        longitude= pos.coords.longitude;
        socket.emit('createLocationMessage' , {
            latitude,
            longitude
        });
        locationButton.removeAttr('disabled').text('Send Location');
    },(err) => {
        alert('unable to fetch the location');
        locationButton.removeAttr('disabled').text('Send Location');
    })
});

socket.on('newLocationMessage' , (body) => {
    let formattedTime = moment(body.createdAt).format('hh:mm a');
    var template = $('#location-message-template').html();
    var html = Mustache.render(template , {
        from: body.from,
        url: body.url,
        createdAt: formattedTime
    });
    $('#messages').append(html);

});